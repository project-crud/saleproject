import { Module } from '@nestjs/common';
import { CatagorysService } from './catagorys.service';
import { CatagorysController } from './catagorys.controller';

@Module({
  controllers: [CatagorysController],
  providers: [CatagorysService],
})
export class CatagorysModule {}
