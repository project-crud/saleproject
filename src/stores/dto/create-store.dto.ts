export class CreateStoreDto {
  store_name: string;

  store_address: string;

  store_tel: string;
}
