import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Store {
  @PrimaryGeneratedColumn()
  store_id: number;
  @Column()
  store_name: string;
  @Column()
  store_address: string;
  @Column()
  store_tel: string;
}
